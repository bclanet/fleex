# Project requirements
- Docker
- Docker compose
- Makefile

# Install dependencies
`make install`

# Start the project
`make start`

# Test the project
`make test`

# Resources
## Zoom credentials
qgrgstcjqjvqhyfwft@nthrw.com / FleexCTO9