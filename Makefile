install:
	cd back && npm i
	cd front && npm i

start:
	docker-compose up --build

test:
	docker-compose -f docker-compose.yml -f docker-compose.test.yml up --build --exit-code-from back back	