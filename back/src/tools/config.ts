interface Config {
    httpPort: number;
    zoom: ZoomConfig;
}

interface ZoomConfig {
    apiKey: string;
    apiSecret: string;
}

export default function getConfig(): Config {
    return {
        httpPort: 8080,
        zoom: {
            apiKey: process.env.ZOOM_API_KEY,
            apiSecret: process.env.ZOOM_API_SECRET,
        },
    };
};