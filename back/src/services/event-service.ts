import { Event } from '../models/event';
import * as zoomService from './zoom-service';

const events: Event[] = [];

export const getEvents = (): Event[] => {
    return events;
};

export const createEvent = async (title: string, start: Date, end: Date): Promise<Event> => {
    const zoomUrl = await zoomService.createMeetingUrl(title);
    const newEvent: Event = {
        start,
        end,
        title,
        zoomUrl,
    };
    events.push(newEvent)
    return newEvent;
};
