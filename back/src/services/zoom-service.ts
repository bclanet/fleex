import axios from 'axios';
import jwt from 'jsonwebtoken';

import getConfig from '../tools/config';

let jwtToken = '';
const getToken = async (): Promise<string> => {
    const zoomConfig = getConfig().zoom;

    return new Promise((resolve) => {
        jwt.verify(jwtToken, zoomConfig.apiSecret, (err) => {
            if (err) {
                const payload = {
                    iss: zoomConfig.apiKey,
                    exp: ((new Date()).getTime() + 5000),
                };
                jwtToken = jwt.sign(payload, zoomConfig.apiSecret);
            }
            resolve(jwtToken);
        });
    });
};

export const createMeetingUrl = async (title: string): Promise<string> => {
    const headers = {
        'authorization': `Bearer ${await getToken()}`,
        'content-type': 'application/json',
    };
    const body = {
        topic: title,
        type: 1,
    };

    const response = await axios.post("https://api.zoom.us/v2/users/me/meetings", body, {
        headers,
    })

    return response.data.join_url;
};