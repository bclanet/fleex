import { Request, Response } from "express";

import * as eventService from '../services/event-service';

export const getEvents = async (req: Request, res: Response) => {
    try {
        res.status(200).json(eventService.getEvents());
    } catch (err) {
        res.status(500).json({
            message: err.message,
        });
    }
};

export const createEvent = async (req: Request, res: Response) => {
    try {
        const { title, start, end } = req.body;
        if (!(typeof title === "string") || title === "") {
            return res.status(422).json({
                message: 'Invalid title',
            });
        }

        const startDate = Date.parse(start);
        if (isNaN(startDate)) {
            return res.status(422).json({
                message: 'Invalid start date',
            });
        }

        const endDate = Date.parse(end);
        if (isNaN(endDate)) {
            return res.status(422).json({
                message: 'Invalid end date',
            });
        }

        if (startDate >= endDate) {
            return res.status(422).json({
                message: 'End date should be greater than start date',
            });
        }

        const newEvent = await eventService.createEvent(title, start, end);
        res.status(200).json(newEvent);
    } catch (err) {
        res.status(500).json({
            message: err.message,
        });
    }
};