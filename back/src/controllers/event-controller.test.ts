import app from '../app';
import supertest from 'supertest';

import * as eventService from '../services/event-service';

describe('GET /events', () => {
    test("should_return_0_event", async () => {
        const response = await supertest(app).get("/events");

        expect(response.statusCode).toBe(200);
        expect(Array.isArray(response.body)).toBeTruthy();
        expect(response.body.length).toEqual(0);
    });

    test("should_return_1_event", async () => {
        const event = await eventService.createEvent('Test', new Date(Date.now()), new Date(Date.now() + 3600));

        const response = await supertest(app).get("/events");

        expect(response.statusCode).toBe(200);
        expect(Array.isArray(response.body)).toBeTruthy();
        expect(response.body.length).toEqual(1);
        expect(response.body[0].title).toBe(event.title);
        expect(response.body[0].zoomUrl).not.toBeNull();
    });
});

describe('POST /events/new', () => {
    test("should_create_an_event", async () => {
        const payload = {
            title: 'Birthday',
            start: new Date(Date.now()),
            end: new Date(Date.now() + 3600),
        };

        const response = await supertest(app).post("/events/new").send(payload);

        expect(response.statusCode).toBe(200);
        expect(response.body.title).toBe(payload.title);
        expect(response.body.zoomUrl).not.toBeNull();
    });

    test("should_fail_creating_an_event__invalid_title", async () => {
        const payload = {
            title: [''],
            start: new Date(Date.now()),
            end: new Date(Date.now() + 3600),
        };

        const response = await supertest(app).post("/events/new").send(payload);

        expect(response.statusCode).toBe(422);
        expect(response.body.message).toBe('Invalid title');
    });

    test("should_fail_creating_an_event__empty_title", async () => {
        const payload = {
            start: new Date(Date.now()),
            end: new Date(Date.now() + 3600),
        };

        const response = await supertest(app).post("/events/new").send(payload);

        expect(response.statusCode).toBe(422);
        expect(response.body.message).toBe('Invalid title');
    });

    test("should_fail_creating_an_event__invalid_start_date", async () => {
        const payload = {
            title: 'Birthday',
            start: 'not_a_date',
            end: new Date(Date.now() + 3600),
        };

        const response = await supertest(app).post("/events/new").send(payload);

        expect(response.statusCode).toBe(422);
        expect(response.body.message).toBe('Invalid start date');
    });

    test("should_fail_creating_an_event__invalid_end_date", async () => {
        const payload = {
            title: 'Birthday',
            start: new Date(Date.now() + 3600),
            end: 'not_a_date',
        };

        const response = await supertest(app).post("/events/new").send(payload);

        expect(response.statusCode).toBe(422);
        expect(response.body.message).toBe('Invalid end date');
    });

    test("should_fail_creating_an_event__start_date_greater_than_end_date", async () => {
        const payload = {
            title: 'Birthday',
            start: new Date(Date.now() + 4000),
            end: new Date(Date.now() + 3600),
        };

        const response = await supertest(app).post("/events/new").send(payload);

        expect(response.statusCode).toBe(422);
        expect(response.body.message).toBe('End date should be greater than start date');
    });

    test("should_fail_creating_an_event__start_date_equal_to_end_date", async () => {
        const date = new Date(Date.now() + 3600);
        const payload = {
            title: 'Birthday',
            start: date,
            end: date,
        };

        const response = await supertest(app).post("/events/new").send(payload);

        expect(response.statusCode).toBe(422);
        expect(response.body.message).toBe('End date should be greater than start date');
    });
});