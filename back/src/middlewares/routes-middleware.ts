import express from "express";

import * as eventController from '../controllers/event-controller';

const router = express.Router();

router.get('/events', eventController.getEvents);
router.post('/events/new', eventController.createEvent);

export default router;