import express from "express";
import cors from "cors";

import routes from "./routes-middleware";

export default function populate(app: express.Application) {
    app.use(express.json());
    app.use(express.urlencoded({
        extended: true
    }));
    app.use(cors());
    app.use(routes);
};