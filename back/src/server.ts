import app from './app';
import getConfig from './tools/config';

const config = getConfig();
app.listen(config.httpPort, () => {
    console.debug('App: started', {
        httpPort: config.httpPort,
    })
});