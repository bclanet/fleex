import React, { useState, useEffect } from 'react';
import { Calendar, momentLocalizer, Views, SlotInfo } from 'react-big-calendar'
import moment from 'moment'
import 'react-big-calendar/lib/css/react-big-calendar.css'

import { Event } from '../models/events';
import * as dateUtils from '../tools/date-utils';
import * as api from '../tools/api';

const localizer = momentLocalizer(moment)

export default function Component() {
    const [events, setEvents] = useState<Event[]>([]);

    useEffect(() => {
        async function fetchData() {
            const newEvents = await api.fetchEvents();
            setEvents(newEvents);
        }
        fetchData();
    });

    const createEvent = async (start: Date, end: Date) => {
        const title = window.prompt(`Event info: ${dateUtils.createPrettyDate(start)} to ${dateUtils.createPrettyDate(end)}
Enter an object...`)
        if (title) {
            await api.createEvent(start, end, title);
            const newEvents = await api.fetchEvents();
            setEvents(newEvents);
        }
    }

    return (
        <Calendar
                selectable
                localizer={localizer}
                events={events.map((e: Event) => ({
                    start: new Date(e.start),
                    end: new Date(e.end),
                    title: `${e.title}\n${e.zoomUrl}`,
                    resource: e.zoomUrl,
                }))}
                defaultView={Views.WEEK}
                defaultDate={new Date()}
                onSelectSlot={(slotInfo: SlotInfo) => {
                    createEvent(new Date(slotInfo.start), new Date(slotInfo.end));
                }}
                onSelectEvent={async event => {
                    await navigator.clipboard.writeText(event.resource);
                    alert("The meeting url has been copied");
                }}
            />
    );
}