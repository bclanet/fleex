import axios from 'axios';

import { Event } from '../models/events';

export const createEvent = async (start: Date, end: Date, title: string): Promise<Event> => {
    const body = {
        start,
        end,
        title,
    };
 
    const response = await axios.post(`${process.env.REACT_APP_API_URL}/events/new`, body);
    
    return response.data;
};

export const fetchEvents = async (): Promise<Event[]> => {
    const response = await axios.get(`${process.env.REACT_APP_API_URL}/events`);
    return response.data;
};