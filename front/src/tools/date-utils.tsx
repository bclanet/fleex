import moment from 'moment'

export const createPrettyDate = (date: Date) => {
    return moment(date).format('LLL')
}