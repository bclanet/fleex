export interface Event {
    title: string;
    start: Date;
    end: Date;
    zoomUrl: string;
};